<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<base href="/mmwa/">
	<meta charset="UTF-8">
	<title>Multimediale Web-Applikationen - Übungen</title>
	<link rel="stylesheet" href="css/formalize.css" type="text/css" media="screen">
	<link rel="stylesheet" href="css/style.css" type="text/css" media="screen, print">
	
	<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery.formalize/1.2/jquery.formalize.min.js"></script>
	<script src="javascript/globals.js"></script>
</head>
<body>
	<div id="page">
		<header>
			<h1>Multimediale Web-Applikationen</h1>
		</header>
		
		<nav>
			<h2>Navigation</h2>
			<ul>
				<li><a href="lesson1">Übung 1</a> - JavaScript 1</li>
				<li><a href="lesson2">Übung 2</a> - JavaScript 2</li>
				<li><a href="lesson3">Übung 3</a> - JavaScript 3</li>
				<li><a href="lesson4">Übung 4</a> - PHP 1</li>
				<li><a href="lesson5">Übung 5</a> - PHP 2</li>
				<li><a href="lesson6">Übung 6</a> - AJAX</li>
			</ul>
		</nav>
		
		<section id="content">
			<?= $content_for_layout; ?>
		</section>
	</div>
</body>
</html>