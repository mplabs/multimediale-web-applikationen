<?php
date_default_timezone_set("Europe/Berlin");

$diff = strtotime('2012-07-13') - time();

$r = strval(rand(0,255));
$g = strval(rand(0,255));
$b = strval(rand(0,255));

$color = '#' . strval(dechex($r)) . strval(dechex($g)) . strval(dechex($b));

?>
<article id="lesson41">
	<h2>Aufgabe 4.1</h2>
	<p>Schreiben Sie ein php-Script, das die verbleibenden Tage bis zum Semester-Ende (13.07.2012, der 195-te Tag des Jahres) ausrechnet und auf dem Bildschirm ausgibt! Sie können dazu die Funktion <code>date()</code> nutzen.</p>
	<div class="solution">
		<p>Verbleibende Tage bis zum Ende des Semesters: <code><?= ceil($diff / 60 / 60 / 24); ?></code></p>
	</div>
</article>

<article id="lesson-42">
	<h2>Aufgabe 4.2</h2>
	<p>Schreiben Sie ein Script, das eine Fläche in zufälliger Farbe auf dem Bildschirm erzeugt! Die Fläche könnten Sie als <?= htmlentities('<div>'); ?>-Container mit einer zufälligen Hintergrundfarbe vereinbaren. Sie müssen Funktionen zur Typ-Konvertierung nutzen!</p>
	<div class="solution" style="width: 100px; height: 100px; background: <?= $color; ?>"></div>
</article>