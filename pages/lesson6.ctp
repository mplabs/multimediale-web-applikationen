<article id="lesson61">
	<h2>Aufgabe 6.1</h2>
	<p>Ändern Sie die Datei <code>ajax01.html</code> so ab, dass ein PHP-Skript auf dem Server ausgeführt und das Ergebnis in die HTML-Datei geladen wird! Denken Sie sich dazu ein (mehr oder weniger) sinnvolles Skript aus!</p>
	<div class="solution">
		<h3>Ein Text wird auf dem Server gelesen und in die HTML-Seite eingefügt.</h3>
		<div id="div1">Platzhalter</div>
		<a href="javascript:return;" id="load1">Text laden...</a>
	</div>
</article>

<article id="lesson62">
	<h2>Aufgabe 6.2</h2>
	<p>Verändern Sie die Datei ajax05-start.html (Programm-Demo) so, dass im Zusammenspiel mit der PHP-Datei ajax05-slideshow.txt (in .php umbenennen) eine einfache Slideshow realisiert wird! Speichern Sie dazu einige oder alle angegebenen Bilddateien in ein Unterverzeichnis "bilder" des HTDOC-Verzeichnisses!</p>
	<div class="solution">
		<form action="#" method="get">
			<div class="input select">
				<label for="faculty">Fakultät</label>
				<select name="faculty" id="faculty" class="input_full">
					<option>Bitte wählen...</option>
					<option value="MN">Mathematik und Naturwissenschaten</option>
					<option value="EI">Elektrotechnik und Informationstechnik</option>
					<option value="IA">Informatik und Automatisierung</option>
					<option value="MB">Maschinenbau</option>
					<option value="WW">Wirtschaftswissenschaften</option>
				</select>
			</div>
			<div class="input select hidden">
				<label for="study">Studiengang</label>
				<select name="study" id="study" class="input_full">
					<option>Bitte wählen...</option>
				</select>
			</div>
		</form>
	</div>
</article>

<script type="text/javascript" src="javascript/lesson6.js"></script>