<?php
$words = array(
	'xml' => "Die Extensible Markup Language (engl. für „erweiterbare Auszeichnungssprache“), abgekürzt XML, ist eine Auszeichnungssprache zur Darstellung hierarchisch strukturierter Daten in Form von Textdaten. XML wird u. a. für den plattform- und implementationsunabhängigen Austausch von Daten zwischen Computersystemen eingesetzt, über das Internet insbesondere.",
	'w3c' => "Das World Wide Web Consortium (kurz: W3C) ist das Gremium zur Standardisierung der das World Wide Web betreffenden Techniken. Es wurde am 1. Oktober 1994 am MIT Laboratory for Computer Science in Cambridge (Massachusetts) gegründet.",
	'rdf' => "Das Resource Description Framework (RDF, engl. (sinngemäß) „System zur Beschreibung von Ressourcen“) bezeichnet eine Familie von Standards des World Wide Web Consortiums (W3C) zur formalen Beschreibung von Informationen über Objekte, sogenannte Ressourcen, die durch eindeutige Bezeichner (URIs) identifiziert werden."
);

switch ($_GET['word']) {
	case 'xml':
		echo $words['xml'];
		break;
	
	case 'w3c':
		echo $words['w3c'];
		break;
		
	case 'rdf':
		echo $words['rdf'];
		break;
	
	default:
		echo false;
		break;
}

?>