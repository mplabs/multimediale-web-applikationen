<?php
header('application/json');

switch ( $_GET['faculty'] ) {
	case 'MN':
		$return = array('Mathematik','Physik','Werkstoffwissenschaften','Angewandte Medienwissenschaft');
		break;
	case 'EI':
		$return = array('Elektrotechnik','Medientechnologie');
		break;
	case 'IA':
		$return = array('Informatik', 'Ingenieurinformatik', 'Biomedizinische Technik');
		break;
	case 'MB':
		$return = array('Maschinenbau', 'Fahrzeugtechnik', 'Mechatronik', 'Optronik');
		break;
	case 'WW':
		$return = array('Medienwirtschaft','Wirtschaftsinformatik','Wirtschaftsingenieurwesen');
		break;
	default:
		$return = array();
}

echo json_encode($return);