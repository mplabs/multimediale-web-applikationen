<article id="lesson-31">
	<h2>Aufgabe 3.1</h2>
	<p>Fügen Sie der Datei Aufgabe01_Vorlage.html einige Events hinzu! Dabei sollen die Begriffe XML, RDF und W3C in Erklärungsfenstern näher erläutert werden. Die Erläuterungen finden Sie in Aufgabe01_Texte.txt.</p>
	<div class="solution">
		<h1>Aufgabe 01</h1>
		<h2>Semantik in XML</h2>
		<p>Die eXtensible Markup Language (XML) entwickelt sich immer mehr zur Universalsprache für die Speicherung von Dokumenten und für den Datenaustausch. XML-Dokumente sind einerseits viel besser strukturierbar als reine ASCII-Texte und andererseits nicht an eine bestimmte Firma oder Software gebunden, weshalb sie universell nutzbar sind. Die Strukturen in Texten und Daten sind in der Regel kein Selbstzweck, sondern dienen der Abgrenzung von inhaltlich zusammengehörigen Teilen gegenüber anderen. Die Tags in XML-Dokumenten eigenen sich dazu in besonderer Weise, da sie sehr flexibel eingesetzt werden können. Man spricht deshalb auch oft von &quot;semantische Tags&quot; oder von &quot;semantische Markup&quot;. Daran ist prinzipiell richtig, dass es möglich ist, Texte und Daten mit XML-Tags zu versehen, die eine eigene Bedeutung besitzen. Diese können dem Dokument semantische Zusatzinformationen verleihen. Es gibt dabei allerdings keinen Automatismus.</p>
		<p>Dieser Artikel hat zum Ziel, das Problem der Semantik in XML-Dokumenten genauer zu beleuchten. Es wird zunächst der semantische Gehalt von reinem XML betrachtet. Dabei wird deutlich, dass auch XML sehr schnell an Grenzen stößt, wenn es um die maschinenverständliche Kodierung von Semantik geht. Deshalb werden mit dem Resource Description Framework (RDF) und den Topic Maps Technologien vorgestellt, die diesbezüglich weit größere Potenzen besitzen. Beide haben die Unterstützung des Semantic Web zum Ziel. Es handelt sich dabei um ein Konzept des World Wide Web Consortiums (W3C) zur Weiterentwicklung des WWW. Durch die Integration von semantischen Informationen sollen die Ressourcen des Internets besser vernetzt, die Suche und Navigation weit zielgerichteter möglich und ganz neue Dienste realisierbar werden. Vieles davon ist heute zwar noch Vision, aber XML und die darauf basierenden Technologien ebnen den Weg dahin.</p>
	</div>
</article>

<article id="lesson-3" class="2">
	<h2>Aufgabe 3.2</h2>
	<p></p>
</article>

<script>
(function () {
	var words = ['xml', 'w3c', 'rdf']
	  , paragraphs = document.getElementById('lesson-31').getElementsByClassName('solution')[0].getElementsByTagName('p');
	
	for (var i = 0; i < words.length; ++i) {
		var word = words[i]
		  , regexp = new RegExp('(' + word + ')', 'ig');
		console.log(regexp);
		for (var j = 0; j < paragraphs.length; ++j) {
			var paragraph = paragraphs[j];
			paragraph.innerHTML = paragraph.innerHTML.replace(regexp, "<dfn title=\"" + word + "\">$1</dfn>");
		}
	}
	
	var getWord = function (word, cb) {
		var xhr = new XMLHttpRequest;
		xhr.open("GET", 'ajax4', true);
		xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
		xhr.send();
		xhr.onreadystatechange = function () {
			if ( xhr.readyState == 4 && xhr.status == 200 ) {
				if ( xhr.responseText == 'false' ) {
					alert("AJAX Error!");
					return;
				}
				cb.call(this, xhr.responseText);
			}
		};
	};
}());
</script>