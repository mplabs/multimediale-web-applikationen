<article id="lesson-21">
	<h2>Aufgabe 2.1</h2>
	<p>Verändern Sie das Script aus Aufgabe 1.4 der letzten Übung, indem Sie die eigentlichen Berechnungen in Funktionen auslagern! Ergänzen Sie die Lösung um eine Möglichkeit, ganzzahlige Exponenten einer Zahl zu berechnen!</p>
	<p><strong>Zusatzaufgabe:</strong> Realisieren Sie (zumindest teilweise) die Eingabe über Bildschirm-Buttons!</p>
	<p>Nutzen Sie dabei die Formularfunktion von HTML:<br>
	<code><?= htmlentities('<form> <input type="button" value="..." onclick="..."/> </form>'); ?></code></p>
</article>

<article id="lesson-23">
	<h2>Aufgabe 2.3</h2>
	<p>Schreiben Sie ein JavaScript-Programm, das ein Objekt <code>student</code> erzeugt, in dem Name, Vorname und Matrikel-Nummer gespeichert werden können! Schreiben Sie eine Methode, die den Namen des Studenten (Vor- und Nachnamen) ausgibt! </p>
	<div class="solution">
		
		<form action="#" method="GET">
			<table id="students-table" width="100%">
				<thead>
					<tr>
						<th>Name</th>
						<th>Vorname</th>
						<th>Matrikel-Nummer</th>
						<th class="tac">Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><input type="text" id="input-name-0" class="input_full"></td>
						<td><input type="text" id="input-first-0" class="input_full"></td>
						<td><input type="text" id="input-number-0" class="input_full"></td>
						<td class="tac"><input type="button" value="Namen ausgeben" id="input-button-0" class="hidden"></td>
					</tr>
				</tbody>
			</table>
		</form>
		<p>Drücken Sie <code>ENTER</code> um Ihre Eingabe zu bestätigen!</p>
	</div>
</article>

<script src="javascript/lesson2.js"></script>