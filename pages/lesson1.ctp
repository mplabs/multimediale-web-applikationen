<article id="lesson-13">
	<h2>Aufgabe 1.3</h2>
	<p>Schreiben Sie <del>unter Verwendung der Meldungsfenster</del> ein Javascript-Programm, das zwei Zahlen einließt und das Produkt als Ergebnis zurückgibt!</p>
	<form action="#" method="get" accept-charset="utf-8">
	  <div class="input text">
	  	<label for="num1">Zahl 1</label>
	  	<input type="text" name="num1" id="num1" class="input_full">
	  </div>
	  <div class="input text">
	  	<label for="num2">Zahl 2</label>
	  	<input type="text" name="num2" id="num2" class="input_full">
	  </div>
	  <div class="input text">
	  	<label for="res1">Produkt</label>
	  	<input type="text" name="res1" id="res1" readonly="readonly" class="input_full">
	  </div>
	</form>
</article>

<article id="lesson-14">
	<h2>Aufgabe 1.4</h2>
	<p>Erweitern Sie das Programm aus Aufgabe 1.3 so, das Sie alle Grundrechenarten ausführen können! Fragen Sie dazu die gewünschte Rechenoperation ab!</p>
	<p><strong>Zusatzaufgabe:</strong> Erweitern Sie den Rechner so, dass er am Ende eine neue Berechnung anbietet und auf Wunsch das vorherige Ergebnis übernimmt!</p>
	<form action="#" method="get" accept-charset="utf-8">
	  <div class="input text">
	  	<label for="num3">Zahl 1</label>
	  	<input type="text" name="num3" id="num2" class="input_full">
	  </div>
	  <div class="input text">
	  	<label for="num4">Zahl 2</label>
	  	<input type="text" name="num3" id="num3" class="input_full">
	  </div>
	  <div class="input select ">
	  	<label for="operation">Operation</label>
	  	<select name="operation" id="operation" class="input_full">
	  		<option value="addition">Addition</option>
	  		<option value="substaction">Substraktion</option>
	  		<option value="division">Division</option>
	  		<option value="multiplication">Multiplikation</option>
	  	</select>
	  </div>
	  <div class="input text">
	  	<label for="res2">Ergebnis</label>
	  	<input type="text" name="res2" id="res2" readonly="readonly" class="input_full">
	  </div>
	</form>
</article>

<article id="lesson-15">
	<h2>Aufgabe 1.5</h2>
	<p>Schreiben Sie ein JS-Programm, mit dem Sie eine ganze Zahl mit Hilfe der Maus über einen Bildschirm-Ziffernblock eingeben können! Benutzen Sie den Befehle <code><?= htmlentities('<form><input type="button" value="Anzeige" onclick="Aktion"/></form>'); ?></code>!
	Die Anwendung könnte wie folgt aussehen.</p>
</article>

<script src="javascript/lesson1.js"></script>