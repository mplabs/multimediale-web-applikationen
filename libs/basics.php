<?php

spl_autoload_register(function ($className) {
    if ( file_exists(LIBS . '/' . $className . '.php') ) {
	require_once LIBS . '/' . $className . '.php';
	return true;
    }
    
    return false;
});