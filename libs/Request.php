<?php

class Request {
    static public function isAjax () {
        if ( isset($_SERVER['HTTP_X_REQUESTED_WITH']) ) {
            return ($_SERVER['HTTP_X_REQUESTED_WITH'] == "XMLHttpRequest"); 
        }
        
        return false;
    }
    
    static public function isJSON () {
        if ( self::isAjax() && isset($_SERVER['HTTP_ACCEPT']) ) {
            return (strpos('application/json', $_SERVER['HTTP_ACCEPT']) !== false);
        }
        
        return false;
    }
}
