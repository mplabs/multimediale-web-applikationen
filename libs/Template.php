<?php

class Template {
    public $layout;
    public $page;

    public function __construct ($layout, $page) {
        $this->layout = $layout;
        $this->page = $page;
    }

    public function getPage () {
	return preg_replace("#\/?(.*)\..+$#", '$1', $this->page);
    }

    public function url ($request, $absolute = false) {
        $request = (!$absolute)
            ? WWW_ROOT . $request
            : $this->_schema() . $_SERVER['HTTP_HOST'] . WWW_ROOT . $request;

        return preg_replace('#(.*)\/+$#', "$1/", $request);
    }

    public function render ($output = true) {
	if ( !file_exists(ROOT . '/pages/' . $this->page) ) {
	    header('Status: 404 Not Found');
	    $this->page = '404.ctp';
	}

        ob_start();
        include ROOT . '/pages/' . $this->page;
        $content_for_layout = ob_get_clean();
        include ROOT . '/layouts/' . $this->layout;
        if ( $output ) {
            ob_end_flush();
        } else {
            return ob_get_flush();
        }
    }

    protected function _schema () {
        return ($_SERVER['SERVER_PORT'] == '80')
            ? 'http://'
            : 'https://';
    }
}