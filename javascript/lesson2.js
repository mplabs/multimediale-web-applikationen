(function () {
	'use strict';
	
	var data = []
	  , tbody = document.getElementsByTagName('tbody')[0]
	  , trTpl = document.getElementsByTagName('tbody')[0]
	  						.getElementsByTagName('tr')[0].cloneNode(true);
	
	var addRow = function (id) {
		var tr = trTpl.cloneNode(true)
		  , inputs = tr.getElementsByTagName('input');
		
		for (var i = 0; i < inputs.length; ++i) {
			var input = inputs[i];
			
			input.id = input.id.replace(/\d+$/, parseInt(id));	
		}
		tbody.appendChild(tr);
	};
	
	var addStudent = function () {
		var row = this.parent
		  , id = this.id.match(/\d+$/);
		
		alert(id);  
	};
	
	document.getElementsByTagName('tbody')[0].onclick = function (e) {
		if ( typeof e.target.type != 'undefined' && e.target.type == 'button' ) {
			var id = parseInt(e.target.id.match(/\d+$/)[0]);
			
			if ( typeof data[id] != 'undefined' ) {
				alert(data[id].firstname + ', ' + data[id].name);
			}
		}
		
		return false;
	};
	
	document.onkeyup = function (e) {
		if ( (typeof e.which != 'undefined' && e.which == 13)
		  && (typeof e.target.tagName != 'undefined' && e.target.tagName == 'INPUT') ) {
			var id = parseInt(e.target.id.match(/\d+$/)[0])
			  , rows = document.getElementsByTagName('tbody')[0].getElementsByTagName('tr')
			  , name = document.getElementById('input-name-' + id)
			  , firstname = document.getElementById('input-first-' + id)
			  , matrikel = document.getElementById('input-number-' + id)
			  , button = document.getElementById('input-button-' + id)
			  , student;

			student = {
				  'name'      : name.value
				, 'firstname' : firstname.value
				, 'matrikel'  : matrikel.value
			};
			
			data[id] = student;
			
			button.className = button.className.replace('hidden', '');

			if ( rows.length == data.length ) {
				addRow(id + 1);
			}
		}
		
		return false;
	};
}());