(function () {
	'use strict';

	var num1 = document.getElementById('num1')
	  , num2 = document.getElementById('num2')
	  , res = document.getElementById('res1');
	
	var product = function () {
		var prod = 1;
		
		for (var i in arguments) {
			prod *= arguments[i];
		}
  		
  		return prod;
	};
	
	num1.onkeyup = num2.onkeyup = function () {
		var val1 = parseInt(num1.value)
		  , val2 = parseInt(num2.value);
		
		if ( !isNaN(val1) && !isNaN(val2) ) {
		  	res.value = product(val1, val2);
		} else {
			res.value = "Input empty or not a number";
		}
	}.debounce(250);

}());