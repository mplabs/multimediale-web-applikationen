(function () {
	'use strict';
	
	var get = function (url, cb) {
		var xhr = new XMLHttpRequest();
		xhr.open("GET", url, true);
		xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
		xhr.send();
		xhr.onreadystatechange = function () {
			if ( xhr.readyState == 4 && xhr.status == 200 ) {
				cb.call(this, xhr.responseText);
			}
		};
	};
	
	document.getElementById('load1').onclick = function () {
		get('ajax1', function (res) {
			document.getElementById('div1').innerHTML = res;
		});
		return false;
	};
	
	
}());