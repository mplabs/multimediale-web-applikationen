<?php

error_reporting(E_ALL ^ E_STRICT);

define('ROOT', ucfirst(str_replace('\\', '/', dirname(__FILE__))));
define('LIBS', ROOT . '/libs');
define('WWW_ROOT', str_replace(ucfirst(str_replace('\\', '/', $_SERVER['DOCUMENT_ROOT'])), '', ROOT));
define('TPL_EXT', '.ctp');

require_once ROOT . '/libs/basics.php';

$layout = (Request::isAjax())
    ? 'ajax' . TPL_EXT
    : 'default' . TPL_EXT;

// Fetch query string
parse_str(parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY), $query);
foreach($query as $key => $val) {
	$_GET[$key] = urldecode($val);
}

// Fetch only the page name
$page = str_replace(WWW_ROOT, '', $_SERVER['REQUEST_URI']);
$page = preg_replace('#(\?.*)$#', "", $page);
$page = ($page === '/')
    ? $page = 'lesson1' . TPL_EXT
    : $page = preg_replace('#\/$#', '', $page) . TPL_EXT;

$template = new Template ($layout, $page);
$template->render();
